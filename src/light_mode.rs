use crate::light_mode::LedMode::{DebugPong, Dim, Kraken, MoreSpooky, Rainbow, Spooky, Unknown};

#[derive(Copy, Clone, Debug)]
pub struct LightMode {
    mode: LedMode
}

#[derive(Copy, Clone, Debug)]
pub enum LedMode {
    Rainbow,
    Spooky,
    MoreSpooky,
    Dim,
    Kraken,

    // Bounces back and forth
    DebugPong,
    Unknown,
}

impl LightMode {
    pub fn new(mode: LedMode) -> Self {
        Self {
            mode
        }
    }

    pub fn get_mode(&self) -> LedMode {
        self.mode
    }

    pub fn set_mode(&mut self, mode: LedMode) {
        self.mode = mode
    }

    pub fn next(&mut self) {
        self.mode = match self.mode {
            Rainbow => {Spooky}
            Spooky => {MoreSpooky}
            MoreSpooky => {Dim}
            Dim => {Kraken}
            Kraken => {Rainbow}
            DebugPong => {DebugPong}
            Unknown => {Unknown}
        }
    }
}
