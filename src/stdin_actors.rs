use crate::light_control_message::LightControlMessage;
use crate::light_control_message::LightControlMessage::{ChangePattern, ToggleSpin};
use crate::light_mode::LedMode::{DebugPong, Dim, Kraken, MoreSpooky, Rainbow, Spooky, Unknown};
use std::char::ParseCharError;
use std::{io, thread};
use tokio::sync::mpsc;
use tokio::sync::mpsc::{Receiver, Sender};

pub struct StdinActor {
    stdin: Receiver<String>,
    sender: Vec<Sender<LightControlMessage>>,
}

fn spawn_stdin_channel() -> Receiver<String> {
    let (tx, rx) = mpsc::channel::<String>(16);
    thread::spawn(move || loop {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        tx.blocking_send(buffer).unwrap();
    });

    rx
}

pub async fn run_stdin_actor(mut actor: StdinActor) {
    loop {
        match actor.stdin.try_recv() {
            Ok(input_text) => {
                let trimmed = input_text.trim();
                match trimmed.parse::<char>() {
                    Ok(ch) => {
                        let next_mode = if ch == '1' {
                            println!("Rainbow fun");
                            Rainbow
                        } else if ch == '2' {
                            println!("Spooky lights!");
                            Spooky
                        } else if ch == '3' {
                            println!("2spooky4me");
                            MoreSpooky
                        } else if ch == '4' {
                            println!("Not very bright are we?");
                            Dim
                        } else if ch == '5' {
                            println!("Let's go Kraken");
                            Kraken
                        } else if ch == 'd' {
                            println!("debug pong mode");
                            DebugPong
                        } else if ch == 's' {
                            for channel in actor.sender.as_slice() {
                                channel.send(ToggleSpin).await.unwrap();
                            }
                            return;
                            // Unknown
                        } else {
                            println!();
                            println!("1. Rainbow");
                            println!("2. Spooky");
                            println!("3. More spooky");
                            println!("4. Dim");
                            println!("d. Debug pong mode");
                            println!();
                            println!("s. Make it spin");
                            Unknown
                        };

                        for channel in actor.sender.as_slice() {
                            channel.send(ChangePattern { pattern: next_mode }).await.unwrap();
                        }
                    }
                    Err(_) => {},
                };
            }
            Err(_) => {}
        }
    }
}

#[derive(Clone)]
pub struct StdinActorHandle {}

impl StdinActorHandle {
    pub fn new(sender: Sender<LightControlMessage>) -> Self {
        let actor = StdinActor {
            stdin: spawn_stdin_channel(),
            sender: vec![sender],
        };

        tokio::spawn(run_stdin_actor(actor));

        Self {}
    }
}
