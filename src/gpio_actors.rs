use rppal::gpio::Level::High;
use rppal::gpio::Trigger::FallingEdge;
use rppal::gpio::{Gpio, InputPin};
use std::collections::HashMap;
use tokio::sync::{mpsc, oneshot};
use crate::light_control_message::LightControlMessage;
use crate::light_control_message::LightControlMessage::ButtonPushedEvent;

struct GpioCallbackData {
    pin: InputPin,
    sender: Vec<mpsc::Sender<LightControlMessage>>,
}

pub struct GpioActor {
    receiver: mpsc::Receiver<GpioActorMessage>,
    // Local state:
    // TODO
    gpio: Gpio,
    tracked_pins: HashMap<u8, GpioCallbackData>,
}

pub enum GpioActorMessage {
    RegisterPullupPin {
        pin: u8,
        channel: mpsc::Sender<LightControlMessage>,
    },
    GetGpioPinValue {
        pin: u8,
        respond_to: oneshot::Sender<u8>,
    },
}

impl GpioActor {
    fn new(receiver: mpsc::Receiver<GpioActorMessage>) -> Self {
        let gpio = Gpio::new().unwrap();

        GpioActor {
            receiver,
            gpio,
            tracked_pins: HashMap::new(),
        }
    }
    fn handle_message(&mut self, msg: GpioActorMessage) {
        match msg {
            GpioActorMessage::RegisterPullupPin { pin, channel } => {
                if self.tracked_pins.contains_key(&pin) {
                    let mut tracked_data = self.tracked_pins.get_mut(&pin);
                    tracked_data.unwrap().sender.push(channel);
                    return;
                }

                let mut p = self.gpio.get(pin).unwrap().into_input_pullup();

                // register the interrupt
                p.set_interrupt(FallingEdge).unwrap();

                self.tracked_pins.insert(
                    pin,
                    GpioCallbackData {
                        pin: p,
                        sender: vec![channel],
                    },
                );
                println!("Registered pullup pin {}", pin);
            }
            GpioActorMessage::GetGpioPinValue { pin, respond_to } => {
                let gpio_level = self.gpio.get(pin).unwrap().read();
                if gpio_level == High {
                    respond_to.send(1).unwrap();
                } else {
                    respond_to.send(0).unwrap();
                }
            }
        }
    }
}

pub async fn run_gpio_actor(mut actor: GpioActor) {
    loop {
        // Handle any incoming messages first
        if let Ok(msg) = actor.receiver.try_recv() {
            actor.handle_message(msg);
        }

        // Poll the pins:
        let pins_to_poll = actor
            .tracked_pins
            .values()
            .map(|a| &a.pin)
            .collect::<Vec<&InputPin>>();

        // println!("Checking interrupts");
        let value = actor
            .gpio
            .poll_interrupts(
                pins_to_poll.as_slice(),
                false,
                Option::from(core::time::Duration::from_millis(50)),
            );
        // println!("value is {:?}", value);

        if value.is_ok() {
            if let Some(v) = value.unwrap() {
                println!("callback triggered pin: {:?} level: {}", v.0.pin(), v.1);
                let data = actor.tracked_pins.get(&v.0.pin()).unwrap();
                for channel in data.sender.as_slice() {
                    channel.send(ButtonPushedEvent {
                        pin: v.0.pin()
                    }).await.unwrap();
                }
            }
        }

    }
}

#[derive(Clone)]
pub struct GpioActorHandle {
    sender: mpsc::Sender<GpioActorMessage>,
}

impl GpioActorHandle {
    pub fn new() -> Self {
        let (sender, receiver) = mpsc::channel(8);
        let actor = GpioActor::new(receiver);
        tokio::spawn(run_gpio_actor(actor));

        Self { sender }
    }

    pub async fn register_pullup_button(
        &self,
        pin: u8,
        send_channel: mpsc::Sender<LightControlMessage>,
    ) {
        let msg = GpioActorMessage::RegisterPullupPin {
            pin,
            channel: send_channel,
        };

        // Ignore send errors. If this send fails, so does the
        // recv.await below. There's no reason to check for the
        // same failure twice.
        let _ = self.sender.send(msg).await;
    }
}
