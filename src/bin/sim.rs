use crate::LedMode::{DebugPong, Dim, MoreSpooky, Rainbow, Spooky};
use colorgrad::Color;
use smart_leds::{SmartLedsWrite, RGB8};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, TryRecvError};
use std::time::{Duration, Instant};
use std::{io, thread};

const NUM_LEDS: usize = 600;
const BRIGHTNESS: f64 = 1.00;

enum LedMode {
    Rainbow,
    Spooky,
    MoreSpooky,
    Dim,

    // Bounces back and forth
    DebugPong
}

fn main() {
    // The LEDs are strung in my room with both 0 index LEDs being right in front of me, over the the window.
    // In order to create one continuous image, the first 300 pixels must be reversed.
    // 300 -> 0, 0 -> 300
    const BLACKBOARD_WALL_PIN: i32 = 21;
    const CRAFT_WALL_PIN: i32 = 18;

    // const DELAY: time::Duration = time::Duration::from_millis(1000);

    let stdin_channel = spawn_stdin_channel();

    let mut blackboard_leds = Ws2812Rpi::new(NUM_LEDS as i32, BLACKBOARD_WALL_PIN).unwrap();
    let mut craft_leds = Ws2812Rpi::new(NUM_LEDS as i32, CRAFT_WALL_PIN).unwrap();

    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    let mut offset = 0;
    let mut use_offset = false;
    let mut led_mode = Rainbow;

    data = rainbow_lights(offset);

    loop {
        data = match stdin_channel.try_recv() {
            Ok(input_text) => {
                let trimmed = input_text.trim();
                match trimmed.parse::<char>() {
                    Ok(ch) => {
                        if ch == '1' {
                            led_mode = Rainbow;
                            println!("Rainbow fun");
                            rainbow_lights(offset)
                        } else if ch == '2' {
                            led_mode = Spooky;
                            println!("Spooky lights!");
                            halloween()
                        } else if ch == '3' {
                            led_mode = MoreSpooky;
                            println!("2spooky4me");
                            better_halloween(offset)
                        } else if ch == '4' {
                            led_mode = Dim;
                            println!("Not very bright are we?");
                            dim(offset)
                        } else if ch == 'd' {
                            println!("debug pong mode");
                            use_offset = true;
                            led_mode = DebugPong;
                            debug_pong(offset)
                        } else if ch == 's' {
                            use_offset = !use_offset;
                            if use_offset {
                                println!("Spin to win")
                            } else {
                                println!("Too dizzy eh?")
                            }
                            data
                        } else {
                            println!();
                            println!("1. Rainbow");
                            println!("2. Spooky");
                            println!("3. More spooky");
                            println!("4. Dim");
                            println!("d. Debug pong mode");
                            println!();
                            println!("s. Make it spin");
                            data
                        }
                    }
                    Err(..) => data,
                }
            }
            Err(TryRecvError::Empty) => {
                if use_offset {
                    match led_mode {
                        Rainbow => rainbow_lights(offset),
                        MoreSpooky => better_halloween(offset),
                        DebugPong => debug_pong(offset),
                        _ => data,
                    }
                } else {
                    data
                }
            }
            Err(TryRecvError::Disconnected) => data,
        };

        let split_data = data.split_at(NUM_LEDS / 2);

        blackboard_leds
            .write(split_data.0.iter().rev().cloned())
            .unwrap();
        craft_leds.write(split_data.1.iter().cloned()).unwrap();

        if use_offset {
            offset = offset + 1;
            if offset >= NUM_LEDS {
                offset = 0
            }
        }

        thread::sleep(Duration::from_millis(25))
    }
}

fn dim(offset: usize) -> [RGB8; 600] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    for (i, led) in data.iter_mut().step_by(4).enumerate() {
        led.r = 32u8;
        led.g = 32u8;
        led.b = 32u8;
    }

    data
}

fn debug_pong(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        if i == offset {
            led.g = 64u8;
            led.r = 0u8;
            led.b = 0u8;
        } else {
            led.r = 0u8;
            led.g = 0u8;
            led.b = 0u8;
        }
    }

    data
}

fn rainbow_lights(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];
    let g = colorgrad::CustomGradient::new()
        .html_colors(&[
            "red", "orange", "yellow", "green", "blue", "indigo", "violet", "red",
        ])
        .domain(&[0.0, NUM_LEDS as f64])
        .build()
        .unwrap();

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let offset_location = ((i + offset) % NUM_LEDS) as f64;

        led.g = (g.at(offset_location).g * 100.0) as u8;
        led.r = (g.at(offset_location).r * 100.0) as u8;
        led.b = (g.at(offset_location).b * 100.0) as u8;
        led.alpha(g.at(offset_location).a as u8);
    }

    data
}

fn better_halloween(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];
    let g = colorgrad::CustomGradient::new()
        .html_colors(&["red", "purple", "red", "purple"])
        .domain(&[0.0, NUM_LEDS as f64])
        .build()
        .unwrap();

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let offset_location = ((i + offset) % NUM_LEDS) as f64;

        led.g = (g.at(offset_location).g * 100.0) as u8;
        led.r = (g.at(offset_location).r * 100.0) as u8;
        led.b = (g.at(offset_location).b * 100.0) as u8;
        led.alpha(g.at(offset_location).a as u8);
    }

    data
}

fn halloween() -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let imod4 = i % 2;

        // Halloween lights
        if imod4 == 0 {
            // Orange
            led.r = (100.0 * BRIGHTNESS) as u8;
            led.g = (12.0 * BRIGHTNESS) as u8;
            led.b = (0.0 * BRIGHTNESS) as u8;
        } else if imod4 == 1 {
            // Purple
            led.r = (14.0 * BRIGHTNESS) as u8;
            led.g = (0.0 * BRIGHTNESS) as u8;
            led.b = (75.0 * BRIGHTNESS) as u8;
        }
    }

    data
}

fn spawn_stdin_channel() -> Receiver<String> {
    let (tx, rx) = mpsc::channel::<String>();
    thread::spawn(move || loop {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        tx.send(buffer).unwrap();
    });
    rx
}
