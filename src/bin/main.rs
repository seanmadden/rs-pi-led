use std::thread;
use std::time::Duration;

use chrono;
use chrono::{Local, Timelike};
use colorgrad::Color;
use smart_leds::{RGB8, SmartLedsWrite};

use LedMode::Unknown;
use rs_pi_led::gpio_actors::GpioActorHandle;
use rs_pi_led::light_control_message::LightControlMessage;
use rs_pi_led::light_control_message::LightControlMessage::{ButtonPushedEvent, ChangePattern, ToggleSpin};
use rs_pi_led::light_mode::{LedMode, LightMode};
use rs_pi_led::light_mode::LedMode::{DebugPong, Dim, Kraken, MoreSpooky, Rainbow, Spooky};
use rs_pi_led::stdin_actors::StdinActorHandle;
use ws281x_rpi::Ws2812Rpi;

const NUM_LEDS: usize = 600;
const BRIGHTNESS: f64 = 1.00;

const DEEP_SEA_BLUE: Color = Color::new(0.0, 22.0, 40.0, 1.0);
const ICE_BLUE: Color = Color::new(153.0, 217.0, 217.0, 1.0);
const BOUNDLESS_BLUE: Color = Color::new(53.0, 84.0, 100.0, 1.0);
const SHADOW_BLUE: Color = Color::new(104.0, 162.0, 185.0, 1.0);
const RED_ALERT: Color = Color::new(233.0, 7.0, 43.0, 1.0);

#[tokio::main]
async fn main() -> Result<(), ()> {
    // The LEDs are strung in my room with both 0 index LEDs being right in front of me, over the the window.
    // In order to create one continuous image, the first 300 pixels must be reversed.
    // 300 -> 0, 0 -> 300
    const BLACKBOARD_WALL_PIN: i32 = 21;
    const CRAFT_WALL_PIN: i32 = 18;

    // const DELAY: time::Duration = time::Duration::from_millis(1000);
    // let (tx, mut rx) = tokio::sync::mpsc::channel(32);

    // let stdin_channel = spawn_stdin_channel();

    let mut blackboard_leds = Ws2812Rpi::new(NUM_LEDS as i32, BLACKBOARD_WALL_PIN).unwrap();
    let mut craft_leds = Ws2812Rpi::new(NUM_LEDS as i32, CRAFT_WALL_PIN).unwrap();

    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    let mut offset = 0;
    let mut use_offset = false;
    let mut light_mode = LightMode::new(Rainbow).to_owned();

    data = rainbow_lights(offset);
    let (light_control_tx, mut light_control_rx) = tokio::sync::mpsc::channel::<LightControlMessage>(32);

    let handle = GpioActorHandle::new();
    let stdin_actor = StdinActorHandle::new(light_control_tx.clone());

    handle.register_pullup_button(19, light_control_tx.clone()).await;

    loop {
        if let message = light_control_rx.try_recv() {
            if message.is_ok() {
                match message.unwrap() {
                    ButtonPushedEvent { pin } => {
                        // println!("Button pushed! {} | current lightMode: {:?}", pin, light_mode);
                        light_mode.next()
                    }
                    ChangePattern { pattern } => {
                        // println!("Changing to pattern {:?}", pattern);
                        light_mode.set_mode(pattern);
                    }
                    ToggleSpin => {
                        use_offset = !use_offset;
                        if use_offset {
                            println!("Spin to win!")
                        } else {
                            println!("A bit dizzy eh?");
                        }
                    }
                }
            }
        }

        data = match light_mode.get_mode() {
            Rainbow => {
                light_mode.set_mode(Rainbow);
                rainbow_lights(offset)
            }
            Spooky => {
                light_mode.set_mode(Spooky);
                halloween()
            }
            MoreSpooky => {
                light_mode.set_mode(MoreSpooky);
                better_halloween(offset)
            }
            Dim => {
                light_mode.set_mode(Dim);
                dim(offset)
            }
            Kraken => {
                light_mode.set_mode(Kraken);
                kraken(offset)
            }
            DebugPong => {
                light_mode.set_mode(DebugPong);
                debug_pong(offset)
            }
            Unknown => {
                data
            }
        };

        let split_data = data.split_at(NUM_LEDS / 2);

        blackboard_leds
            .write(split_data.0.iter().rev().cloned())
            .unwrap();
        craft_leds.write(split_data.1.iter().cloned()).unwrap();

        if use_offset {
            offset = offset + 1;
            if offset >= NUM_LEDS {
                offset = 0
            }
        }

        let current_time = Local::now().time();
        if current_time.hour() >= 21 || current_time.hour() < 6 {
            light_mode.set_mode(Dim);
            data = dim(offset);
        }

        thread::sleep(Duration::from_millis(25))
    }
}

fn dim(offset: usize) -> [RGB8; 600] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    let brightness = 32u8;

    for (i, led) in data.iter_mut().step_by(4).enumerate() {
        led.r = brightness;
        led.g = brightness;
        led.b = brightness;
    }

    data
}

fn debug_pong(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        if i == offset {
            led.g = 64u8;
            led.r = 0u8;
            led.b = 0u8;
        } else {
            led.r = 0u8;
            led.g = 0u8;
            led.b = 0u8;
        }
    }

    data
}

fn rainbow_lights(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];
    let g = colorgrad::CustomGradient::new()
        .html_colors(&[
            "red", "orange", "yellow", "green", "blue", "indigo", "violet", "red",
        ])
        .domain(&[0.0, NUM_LEDS as f64])
        .build()
        .unwrap();

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let offset_location = ((i + offset) % NUM_LEDS) as f64;

        led.g = (g.at(offset_location).g * 100.0) as u8;
        led.r = (g.at(offset_location).r * 100.0) as u8;
        led.b = (g.at(offset_location).b * 100.0) as u8;
        led.alpha(g.at(offset_location).a as u8);
    }

    data
}

fn kraken(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];
    let g = colorgrad::CustomGradient::new()
        .colors(&[
            DEEP_SEA_BLUE,
            ICE_BLUE,
            BOUNDLESS_BLUE,
            SHADOW_BLUE,
            RED_ALERT,
            SHADOW_BLUE,
            BOUNDLESS_BLUE,
            ICE_BLUE,
            DEEP_SEA_BLUE,
        ])
        .domain(&[0.0, NUM_LEDS as f64])
        .build()
        .unwrap();

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let offset_location = ((i + offset) % NUM_LEDS) as f64;

        led.g = (g.at(offset_location).g * 0.05) as u8;
        led.r = (g.at(offset_location).r * 0.05) as u8;
        led.b = (g.at(offset_location).b * 0.05) as u8;
        // led.alpha(g.at(offset_location).a as u8);
    }

    data
}

fn better_halloween(offset: usize) -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];
    let g = colorgrad::CustomGradient::new()
        .html_colors(&["red", "purple", "red", "purple"])
        .domain(&[0.0, NUM_LEDS as f64])
        .build()
        .unwrap();

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let offset_location = ((i + offset) % NUM_LEDS) as f64;

        led.g = (g.at(offset_location).g * 100.0) as u8;
        led.r = (g.at(offset_location).r * 100.0) as u8;
        led.b = (g.at(offset_location).b * 100.0) as u8;
        led.alpha(g.at(offset_location).a as u8);
    }

    data
}

fn halloween() -> [RGB8; NUM_LEDS] {
    let mut data: [RGB8; NUM_LEDS] = [RGB8::default(); NUM_LEDS];

    for (i, led) in data.iter_mut().step_by(1).enumerate() {
        let imod4 = i % 2;

        // Halloween lights
        if imod4 == 0 {
            // Orange
            led.r = (100.0 * BRIGHTNESS) as u8;
            led.g = (12.0 * BRIGHTNESS) as u8;
            led.b = (0.0 * BRIGHTNESS) as u8;
        } else if imod4 == 1 {
            // Purple
            led.r = (14.0 * BRIGHTNESS) as u8;
            led.g = (0.0 * BRIGHTNESS) as u8;
            led.b = (75.0 * BRIGHTNESS) as u8;
        }
    }

    data
}