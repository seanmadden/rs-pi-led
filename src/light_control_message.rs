use crate::light_mode::LedMode;

/**
  * Messages that are to be sent to the "main" tokio event loop in order to control the lights
*/
pub enum LightControlMessage {
    ChangePattern {
        pattern: LedMode
    },
    ButtonPushedEvent {
        pin: u8,
    },
    ToggleSpin,
}